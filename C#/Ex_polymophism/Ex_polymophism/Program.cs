﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_polymophism
{
    public class Shape
    {
        public double Width { get; set; }
        public double Height { get; set; }

        public virtual void Area()
        {
            Console.WriteLine("Dien tich");    
        }
    }
    public class Rectangle:Shape
    {
      

        public Rectangle(double width, double height)
        {
            Width = width;
            Height = height;
        }
        //override
        public override void Area() {
            Console.WriteLine("Dien tich Rec:" + Width * Height);
        }

        //overload method
        public void Size(double sameSize)
        {
            Width = sameSize;
            Height = sameSize;
        }
        public void Size(double width, double height)
        {
            Width = width;
            Height = height;
        }
        //overload operator
        public static Rectangle operator +(Rectangle a, Rectangle b)
        {
            return new Rectangle(a.Width + b.Width, a.Height + b.Height);
        }
        public static Rectangle operator -(Rectangle a, Rectangle b)
        {
            return new Rectangle(a.Width - b.Width, a.Height - b.Height);
        }

    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Rectangle rectangle = new Rectangle(5.0, 6.0);
            rectangle.Size(5.0, 6.0);
            rectangle.Size(5.0);

            Console.WriteLine("Width = {0}, Height = {1}", rectangle.Width, rectangle.Height);
            rectangle.Area();

            //operator +
            Rectangle rectangle1 = new Rectangle(1.0, 2.0);
            Rectangle rectangle2 = new Rectangle(2.0, 3.0);

            Rectangle rectangle3 = rectangle1 + rectangle2;
            Console.WriteLine("Width = {0}, Height = {1}", rectangle3.Width, rectangle3.Height);



        }
    }
}
