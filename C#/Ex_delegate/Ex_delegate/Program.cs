﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_delegate
{
    public class Student
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime birth { get; set; }
        public string gender { get; set; }
            
        public void getId()
        {
            Console.WriteLine("Get Id:" + id);
        }
        public void getName()
        {
            Console.WriteLine("Get Name:" + name);
        }
        public void getBirth()
        {
            Console.WriteLine("Get Birth:" + birth);
        }
        public void getGender()
        {
            Console.WriteLine("Get Gender:" + gender);
        }
    }
    public delegate void MyDelegate();
    internal class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            Console.WriteLine("Nhap Id:");
            student.id = int.Parse(Console.ReadLine());
            Console.WriteLine("Nhap Name:");
            student.name = Console.ReadLine();
            Console.WriteLine("Nhap Birthday:");
            student.birth = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Nhap Gender:");
            student.gender = Console.ReadLine();


            MyDelegate myDelegate = new MyDelegate(student.getName);
            myDelegate();
            MyDelegate myDelegate1 = new MyDelegate(student.getBirth);
            myDelegate1();
            MyDelegate myDelegate2 = new MyDelegate(student.getGender);
            myDelegate2();
            MyDelegate myDelegate3 = new MyDelegate(student.getId);
            myDelegate3();

        }
    }
}
