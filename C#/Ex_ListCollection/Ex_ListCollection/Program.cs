﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//    1. Tạo một class Movie có chứa 2 property bao gồm: Id, Name, Director
//2. Tạo class PlayMovie và bên trong tạo phương thức ShowMovie()
//a.thêm vào tối thiểu 4 phần tử vào List Movie(nhớ thêm phần tử với Id lộn xộn nhé)
//b.xóa bất kỳ 1 phần tử nào trong List
//c.chèn một phần tử vào index = 2;
//    d.sắp xếp các thứ tự giảm dần trong list theo Id.
//   e.tìm phần tử với có Name giá trị là Movie4.
namespace Ex_ListCollection
{
    public class PlayMovie
    {
        public void ShowMovie()
        {

        }
    }
    public class Movie
    {
        private int id;
        private string name;
        private string director;
        public Movie(int _id, string _name, string _director)
        {
            id = _id;
            name = _name;
            director = _director;
        }
        public string Director
        {
            get { return director; }
            set { director = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


    }
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Movie> movies = new List<Movie>();
           
            movies.Add(new Movie(2, "Dog", "MrD"));
            movies.Add(new Movie(1, "Cat", "MrC"));
            movies.Add(new Movie(5, "Elephant", "MrE"));
            movies.Add(new Movie(4, "Bird", "MrB"));
            movies.Add(new Movie(6, "Movie4", "MrB"));

            foreach (var movie in movies)
            {
                Console.WriteLine(movie.Name);
            }
            movies.RemoveAt(2);
            Console.WriteLine("-------");
            //2c
            movies.Insert(2, new Movie(8, "Goat", "MrG"));
            //2d
            var arrangeMovies =movies.OrderByDescending(x=>x.Id);
            //2e
            Movie movieFind = movies.Where(x => x.Name == "Movie4").FirstOrDefault();
            Console.WriteLine(movieFind.Name);
               
            foreach (var movie in arrangeMovies)
            {
                Console.WriteLine(movie.Name);
            }
        }
    }
}
