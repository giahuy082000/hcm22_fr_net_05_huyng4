﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_IENumerable
{
    public class Module
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string EmployeeId { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-mm-dd}")]
        public DateTime Date { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-mm-dd}")]
        public DateTime CompletedDate { get; set; }



        private string status;
        public string Status
        {
            get { return status; }
            set
            {
                if (value != "Active" || value != "Pending" || value != "Inactive") return;
                else Status = value;
            }
        }
       
        public Module(Guid _id,string _name,string _EmployeeId,DateTime _date,DateTime _completedDate,string _status)
        {
            Id = _id;
            Name = _name;
            EmployeeId = _EmployeeId;
            Date = _date;   
            CompletedDate = _completedDate;
            status = _status;
        }

      

    }
}
