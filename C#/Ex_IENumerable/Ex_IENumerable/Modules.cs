﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_IENumerable
{
    public class Modules<T>:IEnumerable<T>
    {
        private  IEnumerable<T> _listModule ;

        public Modules()
        {

        }
        public Modules(T[] listModules)
        {
            _listModule = listModules;
        }
        public void Add(T module)
        {
            _listModule.Append(module); 
        }
       
        public IEnumerator<T> GetEnumerator()
        {
            return _listModule.GetEnumerator();
        }

    

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
