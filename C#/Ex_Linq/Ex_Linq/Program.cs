﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_Linq
{
    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int IDClass { get; set; }

        public Student(string name, int age, int idClass)
        {
            Name = name;
            Age = age;
            IDClass = idClass;
        }

    }

    public class Class
    {
        public int IDClass { get; set; }
        public string Name { get; set; }

        public Class(int idClass, string name)
        {
            IDClass = idClass;
            Name = name;
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Class> classes = new List<Class>()
            {

                new Class(1,"Class 1"),
                new Class(2,"Class 2"),
                new Class(3,"Class 3"),
                new Class(4,"Class 4"),
            };
            List<Student> students = new List<Student>()
            {
                new Student("Hoang",19,4),
                new Student("Huu",19,2),
                new Student("Bao",19,2),
                new Student("Le",19,3),
                new Student("Song",19,1),
            };

            //danh sach hoc sinh theo lop


            var groupJoinClass = classes.GroupJoin(students, c => c.IDClass, s => s.IDClass, (c, s) => new
            {
                Classes = c.Name,
                CountStudent = s.Count(),
                Students = s,
            });

            foreach (var c in groupJoinClass)
            {
                Console.Write(c.Classes);
                Console.WriteLine(" So luong:" + c.CountStudent);
                foreach (var s in c.Students)
                {
                    Console.WriteLine("Ten:" + s.Name);
                    Console.WriteLine("Tuoi:" + s.Age);

                }

            }
        }
    }
}