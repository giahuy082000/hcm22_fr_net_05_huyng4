﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_ListAndDictionary
{
    public class Student:IComparable<Student>
    {
        public int StudentID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Class { get; set; }   
        public Student(int _studentId,string _name,int age,string _class)
        {
            StudentID = _studentId; 
            Name = _name;   
            Age = age;
            Class = _class;
        }

        public int CompareTo(Student other)
        {
            throw new NotImplementedException();
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            //2
            List<Student> students = new List<Student>();   
             students.Add(new Student(1,"Huy",18,"4A"));
             students.Add(new Student(2,"Hoang",18,"4A"));
             students.Add(new Student(3,"Quoc",18,"4A"));
             students.Add(new Student(4,"Quynh",32,"4A"));
             students.Add(new Student(5,"He",32,"4A"));


            Student st = new Student(5, "He", 32, "4A");
            //3
            Console.WriteLine(students.Contains(st));

            //4
            var studentDes = students.OrderByDescending(x => x.StudentID);
           //5
            var studentDelete = students.SingleOrDefault(x => x.StudentID == 1);
            students.Remove(studentDelete);
            Console.WriteLine("So luong student" + students.Count);
            foreach (var item in studentDes)
            {
                Console.WriteLine(item.Name);
            }
           
        
        }
    }
}
