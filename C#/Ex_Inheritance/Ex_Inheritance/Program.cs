﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_Inheritance
{
    public abstract class PaymnetInfomation
    {
        private int _paymentNo { get; set; }
        public virtual void GetPaymentNo() {
            Console.WriteLine("Payment No:" + _paymentNo);
        }
        
     
       
    }
    public interface IOrder
    {
        void PlaceOrder();
        void CancelOrder();
        void GetOrder();
    }
    public class PizzaOrder : PaymnetInfomation, IOrder
    {
        private int _price = 5000;
        private int _amount = 0;
        private bool _isOrder = false;
        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

      
        
        public PizzaOrder(int price, int amount, bool isOrder)
        {
            
            _price = price;
            _amount = amount;
            _isOrder = isOrder;
        }
        public void PlaceOrder()
        {
            if (_amount == 0)
            {
                Console.WriteLine("Can khoi tao amount > 0");
            }
            if (_isOrder == true)
            {
                Console.WriteLine("Ban da dat roi");

            }
            else
            {
                base.GetPaymentNo();
                Console.WriteLine("Tong tien:"+_price*_amount);
                _isOrder = true;
            }
           
        }
        public void CancelOrder() {
            if (_isOrder == true)
            {
                Console.WriteLine("Huy thanh cong");
                _isOrder = false;
            }
            else
            {
                Console.WriteLine("Chua dat hang");
            }
        }
        public void GetOrder() {
            Console.WriteLine("price:"+_price);
            Console.WriteLine("amount:"+_amount);
            Console.WriteLine("isOrder:"+_isOrder);
            GetPaymentNo();
            Console.WriteLine("Tong tien:" + _price*_amount);



        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            PizzaOrder pizzaOrder = new PizzaOrder(100,10,true);
            pizzaOrder.PlaceOrder();
            pizzaOrder.GetOrder();
            pizzaOrder.CancelOrder();
         
        }
    }
}
