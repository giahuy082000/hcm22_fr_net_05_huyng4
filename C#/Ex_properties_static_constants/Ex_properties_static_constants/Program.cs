﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_properties_static_constants
{
    public class Employee
    {

        private int Id;
        private string Name;
        private int Salary;
        private string Role;
        private static int Count_Check = 0;
        private const int pay = 4000;


        public int MyId
        {
            get
            {
                Count_Check++;

                return Id;
            }
            set
            {

                Count_Check++;

                Id = value;
            }
        }
        public string MyName
        {
            get
            {

                Count_Check++;

                return Name;
            }
            set
            {

                Count_Check++;

                Name = value;
            }
        }
        public string MyRole
        {
            get
            {


                Count_Check++;

                return Role;
            }
            set
            {

                Count_Check++;

                Role = value;
            }
        }
        public int MySalary
        {
            get
            {

                Count_Check++;

                return Salary;
            }
            set
            {
                if (Role != "Admin")
                {
                    Count_Check += 40;
                }
                else
                {
                    Count_Check++;
                    Salary = value;
                }

                if (Count_Check > 40)
                {
                    Console.WriteLine("Anh qua kha nghi ! Moi anh len van phong nop tien phat");
                    int tienphat = pay + (Count_Check / 40);
                    Console.WriteLine("so tien phat đuoc tinh = " + tienphat);
                }

            }
        }

        public int MyCountCheck
        {
            get
            {

                return Count_Check;
            }

        }

        public int MyPay
        {
            get
            {
                return pay;
            }
        }




        public Employee(int id, string name, int salary, string role)
        {
            Id = id;
            Name = name;
            Salary = salary;
            Role = role;


        }
    }
    internal class Program
    {

        static void Main(string[] args)
        {
            //Employee emp = new Employee(1, "Boss", 27000, "Admin");
            Employee emp1 = new Employee(2, "Employee", 27000, "Emp");

            Console.WriteLine(emp1.MyName);
            Console.WriteLine(emp1.MySalary);


            emp1.MySalary = 3000;

            Console.WriteLine("Count Check = " + emp1.MyCountCheck);

            Console.ReadKey();






        }
    }

}
