﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student(1,"Huy",DateTime.Parse("2000-01-01"),10,Convert.ToDecimal(9.5),10);
            student.Graduate();
            Console.WriteLine(student.GetCertificate());
            Console.ReadKey();
          
        }
    }
}
