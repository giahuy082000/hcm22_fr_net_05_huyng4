﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    public enum GraduateLevel { Excellent, VeryGood, Good, Average, Failed };
    public class Student : IGraduate
    {
        //Fields
        private int id;
        private string name;
        private DateTime startDate;
        private decimal sqlMark;
        private decimal csharpMark;
        private decimal dsaMark;
        private decimal gpa;
        private GraduateLevel graduateLevel;

        public GraduateLevel GraduateLevel
        {
            get { return graduateLevel; }

        }
        public decimal GPA
        {
            get { return gpa; }

        }
        public decimal DsaMark
        {
            get { return dsaMark; }
            set { dsaMark = value; }
        }

        public decimal CsharpMark
        {
            get { return csharpMark; }
            set { csharpMark = value; }
        }

        public decimal SqlMark
        {
            get { return sqlMark; }
            set { sqlMark = value; }
        }


        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }


        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        //Constructor
        public Student(int _id, string _name, DateTime _startdate, decimal _sqlMark, decimal _csharpMark, decimal _dsaMark)
        {
            id = _id;
            name = _name;
            startDate = _startdate;
            sqlMark = _sqlMark;
            csharpMark = _csharpMark;
            dsaMark = _dsaMark;
          
           
        }
        public void Graduate()
        {
            // Computing GPA
            gpa = (sqlMark + csharpMark + dsaMark) / 3;
            
            if (gpa >= 9)
            {
                graduateLevel = GraduateLevel.Excellent;
            }
            if (gpa >= 8 && gpa < 9)
            {
                graduateLevel = GraduateLevel.VeryGood;
            }
            if (gpa >= 7 && gpa < 8)
            {
                graduateLevel = GraduateLevel.Good;
            }
            if (gpa >= 5 && gpa < 7)
            {
                graduateLevel = GraduateLevel.Average;
            }
            if (gpa < 5)
            {
                graduateLevel = GraduateLevel.Failed;
            }

        }

        public string GetCertificate()
        {
            string outputCertificate = "Name: "+name+", SqlMark: "+ sqlMark+", CsharpMark: "+csharpMark+", DsaMark: "+dsaMark+", GPA: "+gpa.ToString("0.00")+",GraduateLevel: "+graduateLevel;
            return outputCertificate;  
        }



    }
}
