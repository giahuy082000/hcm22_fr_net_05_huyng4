﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] inputArray = { 1, 2, 5, -4, 3 };
            int subLength = 3;
            Console.WriteLine("the maximum contiguous subarray sum: "
                               + FindMaxSubArray(inputArray, subLength));
            Console.ReadKey();

        }


        static int FindMaxSubArray(int[] inputArray, int subLength)
        {
            //init sum
            int sumSubMax = 0;
            //Get first sub
            for (int index = 0; index < subLength; index++)
            {
                sumSubMax += inputArray[index];    
            }

            for(int i = 1;i <= inputArray.Length - subLength; i++)
            {
                int sum = 0;
                //sum from 1
                for (int j = i; j < subLength; j++)
                {
                    sum+=inputArray[j];
                }
                //Compare 2 two sum
                if(sum > sumSubMax)
                {
                    sumSubMax = sum;
                   
                }
            

            }
            return sumSubMax;
            
        }


    }
}
