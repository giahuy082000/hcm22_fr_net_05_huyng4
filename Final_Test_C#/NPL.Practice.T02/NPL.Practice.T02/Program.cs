﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string content = "One of the world's biggest festivals hit the streets of London";
            string contentSecond = "One of the world's biggest festivals hit the  streets of London";
            
            int maxLength = 50;

            Console.WriteLine("Output:"+GetArticleSummary(content,maxLength));
            Console.ReadKey();

        }
        static public string GetArticleSummary(string content, int maxLength)
        {
            //get 50 characters from position 0
            string subStringMaxLength = content.Substring(0, maxLength);
            //get position space
            int lastSpace = subStringMaxLength.LastIndexOf(' ');

            //get sub string from 0 to space position
            string subString = content.Substring(0,lastSpace);
            
            //get result + "..."
            string outputString = subString.Trim() + "...";
            return outputString;


        }
    }
}
